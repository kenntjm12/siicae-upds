<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  






<!-- Web Fonts -->
<link href='https://fonts.googleapis.com/css?family=Ek+Mukta:400,600,700' rel='stylesheet' type='text/css'>	<link href='https://fonts.googleapis.com/css?family=Niconne' rel='stylesheet' type='text/css'>

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/app.css">
<link rel="stylesheet" href="/css/blocks.css">
<link rel="stylesheet" href="/css/one.style.css">

<!-- CSS Footer -->

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="/plugins/animate.css">
<link rel="stylesheet" href="/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/plugins/owl-carousel2/assets/owl.carousel.css">
<link rel="stylesheet" href="/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
<link rel="stylesheet" href="/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">
<link rel="stylesheet" href="/plugins/hover-effects/css/custom-hover-effects.css">
<link rel="stylesheet" href="/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
<link rel="stylesheet" href="/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
<link rel="stylesheet" href="/plugins/pace/pace-flash.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="/css/courses.style.css">
<link rel="stylesheet" href="/css/global.css">
<link rel="stylesheet" href="/css/css-rtl/theme-colors/aqua.css" id="style_color">


<!-- CSS Customization -->
<link rel="stylesheet" href="/css/custom.css">  




 <!--     Fonts and icons     -->
 <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="/css/material-dashboard.css?v=1.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="/demo/demo.css" rel="stylesheet" />



</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('APP_NAME', 'WaterCompare') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <li class="clearfix">
								<ul class="list-inline hidden-md pull-md-right nav__socials">
                                    <a type="button" href="{{ url('/') }}" class="btn btn-info"><i class="icon-custom icon-sm rounded-x fa fa-home"></i> Home</a>
									<a type="button" href="{{ url('/statistics') }}"class="btn btn-success"><i class="icon-custom icon-sm rounded-x fa fa-bar-chart"></i> Estadísticas</a>
									<a type="button" href="{{ url('/log') }}"class="btn btn-warning"><i class="icon-custom icon-sm rounded-x fa fa-exclamation-triangle"></i> Administración</a>
                                    <a type="button" href="{{ url('/user') }}"class="btn btn-danger"><i class="icon-custom icon-sm rounded-x fa fa-user"></i> Mi Cuenta</a>
									<a type="button" href="{{ url('/blog') }}"class="btn btn-secondary"><i class="icon-custom icon-sm rounded-x fa fa-book"></i> Blog</a>					
					    @if (Auth::guest())
                        
                            <a type="button" class="btn " style="background:#9000d4; color:#fff" href="{{ url('/login') }}"><i class="icon-custom icon-sm rounded-x fa fa-sign-in"></i>Ingresar</a>
                            <a type="button" class="btn btn-primary " href="{{ url('/register') }}"><i class="icon-custom icon-sm rounded-x fa fa-user"></i> Registrar</a>
                        </ul>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
