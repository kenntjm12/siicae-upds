@extends('layouts.app')

@section('content')
<div class="container">
    


<body class="light-edition">
  <div class="wrapper ">
    
    <div >
     
      <div class="content">

      
      <div class="navbar-wrapper collapse navbar-collapse" style="text-align:center;">
<div id="exTab1" class="container">	
<ul  class="nav nav-pills">
			<li class="active">
            <a  href="#1a" type="button" class="btn btn-primary" data-toggle="tab">Usuarios</a>
			</li>
			<li>
            <a href="#2a" type="button" class="btn btn-primary" data-toggle="tab">Productos</a>
			</li>
			<li>
            <a href="#3a" type="button" class="btn btn-primary" data-toggle="tab">Comentarios</a>
			</li>
  		    <li>
              <a href="#4a" type="button" class="btn btn-primary" data-toggle="tab">Reportes</a>
			</li>
		</ul>

			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="1a">
          
              
              <section id="usuarios" class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Lista de Usuarios</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          Nombre
                        </th>
                        <th>
                          Edad
                        </th>
                        <th>
                          Sexo
                        </th>
                        <th>
                          E-mail
                        </th>
                        <th>
                          Password
                        </th>
                        <th>
                          Tipo-Rol
                        </th>
                        <th>
                          Estado
                        </th>
                        <th>
                          Acciones
                        </th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            1
                          </td>
                          <th>
                            nombre
                          </th>
                          <th>
                            edad
                          </th>
                          <th>
                           masculino
                          </th>
                          <th>
                            correo
                          </th>
                          <th>
                            password
                          </th>
                          <th>
                            rol
                          </th>
                          <th>
                            estado
                          </th>
                          <td >
                            <button type="button" class="btn" style="background:#28a745"><i class="material-icons">border_color</i></button>
                            <button type="button" class="btn" style="background:#ffc107"><i class="material-icons">remove_red_eye</i></button>
                          </td>
                        </tr>                                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </section>


				</div>
				<div class="tab-pane" id="2a">
         
                <section id="productos" class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Lista de Productos</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          Nombre
                        </th>
                        <th>
                          Marca
                        </th>
                        <th>
                          Presentación
                        </th>
                        <th>
                          Precio
                        </th>
                        <th>
                          Imagen
                        </th>
                        <th>
                          Descripción
                        </th>
                        <th>
                          Cantidad
                        </th>
                        <th>
                          Puntuación
                        </th>
                        <th>
                          Estado
                        </th>
                        <th>
                          Sabor
                        </th>
                        <th>
                          Acciones
                        </th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            1
                          </td>
                          <th>
                            nombre
                          </th>
                          <th>
                            marca
                          </th>
                          <th>
                            presentacion
                          </th>
                          <th>
                            precio
                          </th>
                          <th>
                           imagen
                          </th>
                          <th>
                            descripcion
                          </th>
                          <th>
                            cantidad
                          </th>
                          <th>
                            puntuacion
                          </th>
                          <th>
                            estado
                          </th>
                          <th>
                            sabor
                          </th>
                          <td >
                            <button type="button" class="btn" style="background:#28a745"><i class="material-icons">border_color</i></button>
                            <button type="button" class="btn" style="background:#ffc107"><i class="material-icons">remove_red_eye</i></button>
                          </td>
                        </tr>                                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </section>

                

				</div>
        <div class="tab-pane" id="3a">
          
        
        <section id="comentarios" class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Lista de Comentarios</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          Fecha
                        </th>
                        <th>
                          ID_Usuario
                        </th>
                        <th>
                          Texto
                        </th>
                        <th>
                          ID_Producto
                        </th>
                        <th>
                          ID_Comparación
                        </th>
                        <th>
                          Estado
                        </th>
                        <th>
                          Reporte
                        </th>
                        <th>
                          Acciones
                        </th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            1
                          </td>
                          <th>
                          00 - 00 - 00 00 : 00 : 00
                          </th>
                          <th>
                            1
                          </th>
                          <th>
                            texto
                          </th>
                          <th>
                            1
                          </th>
                          <th>
                            1
                          </th>
                          <th>
                            estado
                          </th>
                          <th>
                            reporte
                          </th>
                          <td >
                            <button type="button" class="btn" style="background:#28a745"><i class="material-icons">border_color</i></button>
                            <button type="button" class="btn" style="background:#ffc107"><i class="material-icons">remove_red_eye</i></button>

                          </td>
                        </tr>                                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </section>


				</div>
          <div class="tab-pane" id="4a">
          
          <section id="reportes" class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Lista de Reportes</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          ID-Usuario
                        </th>
                        <th>
                          ID-Comentario
                        </th>
                        <th>
                          Motivo
                        </th>
                        <th>
                          Fecha
                        </th>
                        <th>
                          Estado
                        </th>
                        <th>
                          Acciones
                        </th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            1
                          </td>
                          <th>
                            1
                          </th>
                          <th>
                            1
                          </th>
                          <th>
                            motivo
                          </th>
                          <th>
                          00 - 00 - 00 00 : 00 : 00
                          </th>
                          <th>
                            estado
                          </th>
                        <td >
                            <button type="button" class="btn" style="background:#28a745"><i class="material-icons">border_color</i></button>
                            <button type="button" class="btn" style="background:#ffc107"><i class="material-icons">remove_red_eye</i></button>

                          </td>
                        </tr>                                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </section>

				</div>
			</div>
  </div>






          
       
      </div>
     
      <script>
        const x = new Date().getFullYear();
        let date = document.getElementById('date');
        date.innerHTML = '&copy; ' + x + date.innerHTML;
      </script>
    </div>
  </div>
 
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="https://unpkg.com/default-passive-events"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.js?v=2.1.0"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
    $(document).ready(function() {
      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
</body>























</div>
@endsection
