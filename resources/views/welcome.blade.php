<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Home Watercompare</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

        	<!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
<link href='https://fonts.googleapis.com/css?family=Ek+Mukta:400,600,700' rel='stylesheet' type='text/css'>	<link href='https://fonts.googleapis.com/css?family=Niconne' rel='stylesheet' type='text/css'>

	<!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/app.css">
  <link rel="stylesheet" href="/css/blocks.css">
  <link rel="stylesheet" href="/css/one.style.css">

	<!-- CSS Footer -->

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="/plugins/animate.css">
	<link rel="stylesheet" href="/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/plugins/owl-carousel2/assets/owl.carousel.css">
	<link rel="stylesheet" href="/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
	<link rel="stylesheet" href="/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">
	<link rel="stylesheet" href="/plugins/hover-effects/css/custom-hover-effects.css">
	<link rel="stylesheet" href="/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
	<link rel="stylesheet" href="/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
	<link rel="stylesheet" href="/plugins/pace/pace-flash.css">

  <!-- CSS Theme -->
  <link rel="stylesheet" href="/css/courses.style.css">
  <link rel="stylesheet" href="/css/global.css">
  <link rel="stylesheet" href="/css/css-rtl/theme-colors/aqua.css" id="style_color">
    

  <!-- CSS Customization -->
  <link rel="stylesheet" href="/css/custom.css">
    </head>


    <body  id="body" data-spy="scroll" data-target=".one-page-header" class="demo-lightbox-gallery font-main promo-padding-top">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    WaterCompare
                </div>
            </div>
        </div>







        <main class="wrapper">
		<!-- Header -->
		<nav class="header one-page-header navbar navbar-default navbar-fixed-top courses-header one-page-nav-scrolling one-page-nav__fixed" data-role="navigation">
			<div class="container-fluid g-pr-40 g-pl-40">
				<div class="menu-container page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<a class="navbar-brand" href="#body">
						<h2>WaterCompare</h2>
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-ex1-collapse g-pt-25">
					<div class="menu-container">
						<ul class="nav navbar-nav">
							<li class="page-scroll home nav__text">
								<a href="#body">Home</a>
							</li>
							
							<li class="page-scroll nav__text">
								<a href="#compare">Comparar</a>
							</li>
							<li class="page-scroll nav__text">
								<a href="#numbers">Our Numbers</a>
							</li>
							<li class="page-scroll nav__text">
								<a href="#gallery">Gallery</a>
							</li>
							<li class="page-scroll nav__text">
								<a href="#newpartner">Registrar Empresa</a>
							</li>
							<li class="page-scroll nav__text">
								<a href="#partners">Asociados</a>
							</li>
							<li class="page-scroll nav__text">
								<a href="#about">Quienes Somos</a>
							</li>
							<li class="clearfix">
								<ul class="list-inline hidden-md pull-md-right nav__socials">
									<li>
									<a type="button" class="btn btn-primary " href="{{ url('/register') }}"><i class="icon-custom icon-sm rounded-x fa fa-user"></i> Registrar</a>
									
									</li>
									<li>
									<a type="button" class="btn " style="background:#9000d4; color:#fff" href="{{ url('/login') }}"><i class="icon-custom icon-sm rounded-x fa fa-sign-in"></i>Ingresar</a>
									</li>
									<li>
									<a type="button" class="btn btn-success"href="{{ url('/statistics') }}"><i class="icon-custom icon-sm rounded-x fa fa-bar-chart"></i> Estadísticas</a>
									</li>
									<li>
									<a type="button" class="btn btn-warning"href="{{ url('/log') }}"><i class="icon-custom icon-sm rounded-x fa fa-exclamation-triangle"></i> Administración</a>
									</li>

<li>
                                <a type="button" href="{{ url('/user') }}"class="btn btn-danger"><i class="icon-custom icon-sm rounded-x fa fa-user"></i> Mi Cuenta</a>
								</li>
								<li>
									<a type="button" href="{{ url('/blog') }}"class="btn btn-secondary"><i class="icon-custom icon-sm rounded-x fa fa-book"></i> Blog</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>
		<!-- End Header -->

		<!-- Promo Block-->
		<section class="promo-section" id="intro">
			<div class="container-fluid g-pr-30 g-pl-30">
				<!-- Owl Carousel v1-->
				<div class="owl2-carousel-v1 owl-theme animated slideInUp">
					<div class="item text-left">
						<div class="course">
							<img class="img-responsive" src="/img-temp/slider1.jpg" alt="">
							<a href="#" class="course__cat g-padding-10">Tag1</a>
							<div class="course--bottom">
								<span class="course__dur">
									<i class="fa course__icon fa-calendar g-mr-10 g-mb-10"></i> 00/00/0000 00:00:00
								</span>
								<h1 class="course__title g-mb-20 font-main"><a href="#">nombre</a></h1>
								<p class="course__text g-mb-30">marca</p>
								<p class="course__text g-mb-30">presentacion</p>
								<h1 class="course__title g-mb-20 font-main">precio</h1>
								<p class="course__text g-mb-30">descripcion</p>
								<p class="course__text g-mb-30">cantidad</p>
								<p class="course__text g-mb-30">puntuación</p>
								<p class="course__text g-mb-30">estado del producto</p>
								<a href="#" class="btn-u course__btn-u">Leer más</a>
							</div>
						</div>
					</div>

					
				</div>
			</div>
		</section>
		<!-- End Promo Block -->
     
        <!-- Our Courses -->
		<section id="compare">
			<div class="container-fluid no-padding pattern-v1">
				<div class="content-lg g-pr-30 g-pl-30">
					<div class="heading-v13 heading-v13--center text-center g-pt-10 g-mb-80">
						<span class="heading-v13__block-name">Comparar</span>
						<h1 class="heading-v13__title font-main">CUal es mejor</h1>
					</div>

					<!-- Owl Carousel v2-->
					<div class="owl2-carousel-v2 " style="text-align:center; padding:10px;margin:10px">
						<div class="item text-left">
							<div class="course-info"style="text-align:center; padding:10px;margin:10px">
							<div class="container">
	<div class="row">
		<div class="col-md-3">
            <div class="input-group" id="adv-search">
			
                <input type="text" class="form-control" placeholder="Search for snippets" />
                <div class="input-group-btn">
				<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>

                    <div class="btn-group" role="group">
                        <div class="dropdown dropdown-lg col-md-3">
                            <button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                            <div class="dropdown-menu dropdown-menu-right " role="menu">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label for="filter">Filtrar por:</label>
                                    <select class="form-control">
                                        <option value="0" selected>Todo</option>
                                        <option value="1">Más Destacado</option>
                                        <option value="2">Más Popular</option>
                                        <option value="3">Más Valorado</option>
                                        <option value="4">Más Comentado</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Nombre</label>
                                    <input class="form-control" type="text" />
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Marca</label>
                                    <input class="form-control" type="text" />
                                  </div>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
          </div>
        </div>
	</div>
								<div class="course-info__block">
									<img class="course-info__img" src="/img-temp/course1.jpg" alt="">
									<a href="#" class="course-info__cat g-padding-10">tag</a>
									<h2 class="course-info__title font-main">
										<a class="course-info__link" href="#">nombre</a>
									</h2>
								</div>
								<div class="course-info__bott g-padding-30">
									<p class="course-info__price g-mb-40">
									<span class="course-info__price--big">precio <strong>Bs.</strong></span> cantidad
									</p>
									<p class="course-info__text g-mb-25">descripcion.</p>
									<span class="course-info__dur g-mb-20 g-dp-block">
										<i class="fa course-info__icon fa-calendar g-mr-10"></i> 00/00/00 00:00:00
									</span>
									<span class="course-info__level">
										<i class="fa course-info__icon fa-certificate g-mr-10"></i> puntuacion
									</span>
								</div>
							</div>
						</div>
<h1>vs</h1>
						<div class="item text-left">
						<div class="course-info"style="text-align:center; padding:10px;margin:10px">
						<div class="container">
	<div class="row">
		<div class="col-md-3">
            <div class="input-group" id="adv-search">
			
                <input type="text" class="form-control" placeholder="Search for snippets" />
                <div class="input-group-btn">
				<button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>

                    <div class="btn-group" role="group">
                        <div class="dropdown dropdown-lg col-md-3">
                            <button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                            <div class="dropdown-menu dropdown-menu-right " role="menu">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label for="filter">Filtrar por:</label>
                                    <select class="form-control">
                                        <option value="0" selected>Todo</option>
                                        <option value="1">Más Destacado</option>
                                        <option value="2">Más Popular</option>
                                        <option value="3">Más Valorado</option>
                                        <option value="4">Más Comentado</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Nombre</label>
                                    <input class="form-control" type="text" />
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Marca</label>
                                    <input class="form-control" type="text" />
                                  </div>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
          </div>
        </div>
	</div>
								<div class="course-info__block">
									<img class="course-info__img" src="/img-temp/course1.jpg" alt="">
									<a href="#" class="course-info__cat g-padding-10">tag</a>
									<h2 class="course-info__title font-main">
										<a class="course-info__link" href="#">nombre</a>
									</h2>
								</div>

								<div class="course-info__bott g-padding-30">
									<p class="course-info__price g-mb-40">
									<span class="course-info__price--big">precio <strong>Bs.</strong></span> cantidad
									</p>
									<p class="course-info__text g-mb-25">descripcion.</p>
									<span class="course-info__dur g-mb-20 g-dp-block">
										<i class="fa course-info__icon fa-calendar g-mr-10"></i> 00/00/00 00:00:00
									</span>
									<span class="course-info__level">
										<i class="fa course-info__icon fa-certificate g-mr-10"></i> puntuacion
									</span>
								</div>
							</div>
	
						</div>

						
					</div>
				</div>
			</div>
		</section>
		<!-- End Our Courses -->

		<!-- Our Numbers -->
		<section id="numbers">
			<div class="container">
				<div class="content-lg g-pb-80 g-pr-30 g-pl-30">
					<div class="heading-v13 g-pl-100 heading-v13--left text-left g-mb-60">
						<div class="row">
							<div class="col-md-6">
								<span class="heading-v13__block-name">Estadísticas</span>
								<h1 class="heading-v13__title font-main g-mb-15">Sucre - Bolivia, 00 de 000000 del 0000, 00:00:00</h1>
							</div>

							<div class="col-md-6">
								<p class="heading-v13__text">descripción resultados.</p>
							</div>
						</div>
					</div>

					<div class="row g-mb-40">
						<div class="counters col-sm-6">
							<div class="row">
								<div class="col-md-4 g-pl-30">
									<div class="counter-circle rounded-x">
										<span class="counter">70</span>
									</div>
								</div>
								<div class="col-md-8 text-left g-pt-40">
									<h4 class="g-mb-10 font-main counters__title">Productos Registrados</h4>
									<p class="counters__text">cada empresa lanza aproximadamente 4 distintas líneas de agua y derivados.</p>
								</div>
							</div>
						</div>

						<div class="counters col-sm-6">
							<div class="row">
								<div class="col-md-4 g-pl-30">
									<div class="counter-circle rounded-x">
										<span class="counter">32</span>
									</div>
								</div>
								<div class="col-md-8 text-left g-pt-40">
									<h4 class="g-mb-10 font-main counters__title">Empresas Participantes</h4>
									<p class="counters__text">2 de cada 15 empresas de la ciudad de Sucre se dedican a la producción y distribución de agua.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="counters col-sm-6">
							<div class="row">
								<div class="col-md-4 g-pl-30">
									<div class="counter-circle rounded-x">
										<span class="counter">2780</span>
									</div>
								</div>
								<div class="col-md-8 text-left g-pt-40">
									<h4 class="g-mb-10 font-main counters__title">Consumidores al Día</h4>
									<p class="counters__text">En promedio, un ciudadano de Sucre consume 4 litros de agua por día, menos que la recomendable.</p>
								</div>
							</div>
						</div>

						<div class="counters col-sm-6">
							<div class="row">
								<div class="col-md-4 g-pl-30">
									<div class="counter-circle rounded-x">
										<span class="counter">192</span>
									</div>
								</div>
								<div class="col-md-8 text-left g-pt-40">
									<h4 class="g-mb-10 font-main counters__title">producto Consumido al Día</h4>
									<p class="counters__text">Se calcula que en un día se produce 3000 kg de basura con los envases desechados.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Our Numbers -->

		<!-- Gallery -->
		<section id="gallery">
			<div class="g-pb-100">
				<div class="container">
					<div class="g-pr-40 g-pl-40">
						<div class="heading-v13 g-pl-100 heading-v13--left text-left g-mb-60">
								<div class="row">
									<div class="col-md-12">
										<span class="heading-v13__block-name">Galería</span>
										<h1 class="heading-v13__title font-main g-mb-15">Participando activamente</h1>
									</div>
								</div>
						</div>
					</div>
				</div>

				<div class="container-fluid">
					<div class="cube-portfolio cube-portfolio--mod">
						<div id="grid-container" class="cbp-l-grid-gallery">
							<div class="cbp-item">
								<a href="/img-temp/gallery1.jpg" class="cbp-caption cbp-lightbox" data-title="	custom title 1">
									<div class="cbp-caption-defaultWrap">
										<img src="/img-temp/gallery1.jpg" alt="">
									</div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Courses for kids</div>
												<p>Nam ullamcorper mauris ex, ut efficitur est convallis nec</p>
											</div>
										</div>
									</div>
								</a>
							</div>

							<div class="cbp-item">
								<a href="/img-temp/gallery2.jpg" class="cbp-caption cbp-lightbox" data-title="custom title 2">
									<div class="cbp-caption-defaultWrap">
										<img src="/img-temp/gallery2.jpg" alt="">
									</div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Cooking class</div>
												<p>Nam ullamcorper mauris ex, ut efficitur est convallis nec</p>
											</div>
										</div>
									</div>
								</a>
							</div>

							<div class="cbp-item">
								<a href="/img-temp/gallery3.jpg" class="cbp-caption cbp-lightbox" data-title="custom title 3">
									<div class="cbp-caption-defaultWrap">
										<img src="/img-temp/gallery3.jpg" alt="">
									</div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Personalized lessons</div>
												<p>Nam ullamcorper mauris ex, ut efficitur est convallis nec</p>
											</div>
										</div>
									</div>
								</a>
							</div>

							<div class="cbp-item">
								<a href="/img-temp/gallery4.jpg" class="cbp-caption cbp-lightbox" data-title="custom title 4">
									<div class="cbp-caption-defaultWrap">
										<img src="/img-temp/gallery4.jpg" alt="">
									</div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Discussions in class</div>
												<p>Nam ullamcorper mauris ex, ut efficitur est convallis nec</p>
											</div>
										</div>
									</div>
								</a>
							</div>

							<div class="cbp-item">
								<a href="/img-temp/gallery5.jpg" class="cbp-caption cbp-lightbox" data-title="custom title 5">
									<div class="cbp-caption-defaultWrap">
										<img src="/img-temp/gallery5.jpg" alt="">
									</div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Gymnastics for elderly</div>
												<p>Nam ullamcorper mauris ex, ut efficitur est convallis nec</p>
											</div>
										</div>
									</div>
								</a>
							</div>

							<div class="cbp-item">
								<a href="/img-temp/gallery6.jpg" class="cbp-caption cbp-lightbox" data-title="custom title 6">
									<div class="cbp-caption-defaultWrap">
										<img src="/img-temp/gallery6.jpg" alt="">
									</div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Arts for elderly</div>
												<p>Nam ullamcorper mauris ex, ut efficitur est convallis nec</p>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Gallery -->

		<!-- Our Offers -->
		<section id="newpartner">
			<div class="container-fluid no-padding pattern-v1">
				<div class="container">
					<div class="content-lg g-pb-90">
						<div class="heading-v13 heading-v13--center heading-v13 text-center g-pt-10 g-mb-60">
							<span class="heading-v13__block-name">Planes de Pago</span>
							<h1 class="heading-v13__title font-main">Registra tu Empresa</h1>
							<h3>Trabaja con nosotros para recibir información sobre el mercado de consumidores y clientes potecniales.</h3>
						</div>
						<!-- Owl Carousel v4-->
						<div class="owl2-carousel-v4 owl-theme dots-v1">
							<div class="item text-center g-mb-40">
								<div class="offer g-padding-50">
									<span class="offer__price g-dp-block">$2</span>
									<span class="offer__price--per g-dp-block g-mb-30">anual</span>
									<h2 class="offer__name font-main g-mb-60">Estándar</h2>
									<p class="offer__text g-mb-30">Ofrecer producto y establecer precios hacia clientes potenciales y nuevos nichos de mercado.</p>
									<a href="#" class="btn-u offer__btn-u">Order Now</a>
								</div>
							</div>

							<div class="item text-center g-mb-40">
								<div class="offer g-padding-50">
									<span class="offer__price g-dp-block">$4</span>
									<span class="offer__price--per g-dp-block g-mb-30">anual</span>
									<h2 class="offer__name font-main g-mb-60">Premium</h2>
									<p class="offer__text g-mb-30">Asesoramiento de resultados, informes mensuales sobre actividades.</p>
									<a href="#" class="btn-u offer__btn-u">Order Now</a>
								</div>
							</div>

							<div class="item text-center g-mb-40">
								<div class="offer g-padding-50">
									<span class="offer__price g-dp-block">$7</span>
									<span class="offer__price--per g-dp-block g-mb-30">anual</span>
									<h2 class="offer__name font-main g-mb-60">Professional</h2>
									<p class="offer__text g-mb-30">Asesoramiento de marketing en las redes sociales, entrege de resultados a un servidor propio.</p>
									<a href="#" class="btn-u offer__btn-u">Order Now</a>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Our Offers -->

		<!-- Our About -->
		<section id="about">
			<div class="container-fluid no-padding pattern-v1--color">
				<div class="container">
					<div class="content-lg">
						<div class="heading-v13 heading-v13--center heading-v13--diff text-center g-pt-10 g-mb-60">
							<span class="heading-v13__block-name">Quienes Somos</span>
							<h1 class="heading-v13__title font-main">Conoce al Equipo Desarrollador</h1>
							<p class="heading-v13__text">Keije community desarrollo este proyecto junto a cursos de distintas carreras de la Universidad Privada Domingo Savio.</p>
						</div>

						<!-- Owl Carousel v3-->
						<div class="owl2-carousel-v3 owl-theme controls-v1">
							<div class="item text-center">
								<div class="teacher g-padding-20">
									<img class="teacher__img g-mb-30" src="/img-temp/team1.jpg" alt="">
									<ul class="teacher__social list-unstyled">
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-pinterest-p"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
									<span class="teacher__speciality g-dp-block g-mb-15">Frontend</span>
									<span class="teacher__name g-dp-block g-mb-15">Jaldin Montes <br> Kenneth</span>
									<p class="teacher__about">Realiza la programación y el diseño de las interfaces de usuario, coordina con los demás integrantes del equipo para promover un diseño atractivo y funcional para el cliente.</p>
								</div>
							</div>

							<div class="item text-center">
								<div class="teacher g-padding-20">
									<img class="teacher__img g-mb-30" src="/img-temp/team2.jpg" alt="">
									<ul class="teacher__social list-unstyled">
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-pinterest-p"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
									<span class="teacher__speciality g-dp-block g-mb-15">Backend</span>
									<span class="teacher__name g-dp-block g-mb-15">Céspedes Ayllón <br> Ismael</span>
									<p class="teacher__about">Es el encargado de la programación interna del sistema en relación máquina servidor, garantizando el correcto funcionamiento interno del sistema.</p>
								</div>
							</div>

							<div class="item text-center">
								<div class="teacher g-padding-20">
									<img class="teacher__img g-mb-30" src="/img-temp/team3.jpg" alt="">
									<ul class="teacher__social list-unstyled">
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-pinterest-p"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
									<span class="teacher__speciality g-dp-block g-mb-15">Base de Datos</span>
									<span class="teacher__name g-dp-block g-mb-15">Juarez <br> Erick</span>
									<p class="teacher__about">Realiza el diseño afinado de la base de datos del sistema y además se encarga de resguardar los datos almacenados, garantizando su seguridad y fácil interpretación.</p>
								</div>
							</div>

							<div class="item text-center">
								<div class="teacher g-padding-20">
									<img class="teacher__img g-mb-30" src="/img-temp/team4.jpg" alt="">
									<ul class="teacher__social list-unstyled">
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-pinterest-p"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
									<span class="teacher__speciality g-dp-block g-mb-15">Tester</span>
									<span class="teacher__name g-dp-block g-mb-15">Reyes Quispe <br> Enrique</span>
									<p class="teacher__about">Aquel encargado de verificar el funcionamiento total del sistema, está al tanto de la atención al cliente, retroalimentando al sistema de acuerdo a problemas o recomendaciones propuestas por los usuarios.</p>
								</div>
							</div>

							<div class="item text-center">
								<div class="teacher g-padding-20">
									<img class="teacher__img g-mb-30" src="/img-temp/team5.jpg" alt="">
									<ul class="teacher__social list-unstyled">
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-pinterest-p"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#" class="teacher__list-item">
												<i class="icon-custom teacher__icon icon-sm fa fa-linkedin"></i>
											</a>
										</li>
									</ul>
									<span class="teacher__speciality g-dp-block g-mb-15">Cook </span>
									<span class="teacher__name g-dp-block g-mb-15">Rebecca <br> Smithmann</span>
									<p class="teacher__about">Mauris sodales tellus vel felis dapibus, sit amet porta nibh egestas. Sed dignissim tellus quis sapien sagittis cursus. Cras porttitor auctor sapien, eu tempus nunc.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Our About -->

		<!-- Our Partners -->
		<section id="partners">
			<div class="container">
				<div class="content-lg g-pr-30 g-pl-30">
					<div class="heading-v13 g-pl-100 heading-v13--left text-left g-mb-60">
						<div class="row">
							<div class="col-md-6">
								<span class="heading-v13__block-name">Our Partners</span>
								<h1 class="heading-v13__title font-main g-mb-15">Who trusts us</h1>
							</div>

							<div class="col-md-6">
								<p class="heading-v13__text">Etiam dolor tortor, egestas a libero eget, sollicitudin maximus nulla. Nunc vitae maximus ipsum. Vestibulum sodales nisi massa, vitae blandit massa luctus id.</p>
							</div>
						</div>
					</div>

					<!-- Partenrs -->
					<div class="partners">
						<ul class="list-unstyled partners__list">
							<li class="partner__cell partner__cell--first">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img01.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--2">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img02.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--3">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img03.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--4">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img04.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--5">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img05.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--6">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img06.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--7">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img07.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--8">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img08.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--9">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img09.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--10">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img10.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--11">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img11.png" alt="">
								</a>
							</li>
							<li class="partner__cell partner__cell--last">
								<a href="#" class="partner__cell--link">
									<img class="img-responsive partner__img" src="/img-temp/partners/img12.png" alt="">
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!-- End Our Partners -->

		<footer class="footer" >
			<div class="container-fluid pattern-v1 no-padding" style="color:#000000">
				<div class="g-pt-60 g-pb-40">
					<div class="container" > 
						<div class="row">
							<div class="col-md-3 col-sm-6 col-xs-8 col-xs-offset-2 col-sm-offset-0 text-left g-pt-10">
								<h1 style="color:#000000">WaterCompare</h1>
								<p class="footer__text g-mb-20">keije community se dedica al estudio y desarrollo de software garantizando un servicio de buena calidad.</p>
								<ul class="list-inline">
									<li>
										<a href="#" class="footer__social">
											<i class="icon-custom icon-sm rounded-x fa fa-twitter"></i>
										</a>
									</li>
									<li>
										<a href="#" class="footer__social">
											<i class="icon-custom icon-sm rounded-x fa fa-pinterest-p"></i>
										</a>
									</li>
									<li>
										<a href="#" class="footer__social">
											<i class="icon-custom icon-sm rounded-x fa fa-facebook"></i>
										</a>
									</li>
								</ul>
							</div>


							<div class="col-md-3 col-sm-6 col-xs-8 col-xs-offset-2 col-sm-offset-0 text-left">
								<h2 class="footer__subtitle font-main">useful links</h2>
								<ul class="list-unstyled">
									<li class="g-mb-5"><a href="" class="footer__ulink">- Fusce dolor libero, efficitur et lobortis</a></li>
									<li class="g-mb-5"><a href="" class="footer__ulink">- Proin fermentum turpis eget nisi</a></li>
									<li class="g-mb-5"><a href="" class="footer__ulink">- Mauris sodales tellus vel felis dapibus</a></li>
									<li class="g-mb-5"><a href="" class="footer__ulink">- Ut pulvinar tellus sed elit luctus</a></li>
									<li class="g-mb-5"><a href="" class="footer__ulink">- Proin sollicitudin turpis in massa rutrum</a></li>
									<li><a href="" class="footer__ulink">- Vestibulum semper</a></li>
								</ul>
							</div>

							<div class="col-md-3 col-sm-6 col-xs-8 col-xs-offset-2 col-sm-offset-0 text-left ">
								<h2 class="footer__subtitle font-main">Some photos</h2>
								<!-- Photostream -->
								<ul class="list-inline blog-photostream">
									<li class="img-hover-v2">
										<a href="/img-temp/gallery1.jpg" class="cbp-caption cbp-lightbox" title="Image 1">
											<span><img class="img-responsive" src="/img-temp/footer__gallery1.jpg" alt=""></span>
										</a>
									</li>
									<li class="img-hover-v2">
										<a href="/img-temp/gallery2.jpg" class="cbp-caption cbp-lightbox" title="Image 2">
											<span><img class="img-responsive" src="/img-temp/footer__gallery2.jpg" alt=""></span>
										</a>
									</li>
									<li class="img-hover-v2">
										<a href="/img-temp/gallery3.jpg" class="cbp-caption cbp-lightbox" title="Image 3">
											<span><img class="img-responsive" src="/img-temp/footer__gallery3.jpg" alt=""></span>
										</a>
									</li>
									<li class="img-hover-v2">
										<a href="/img-temp/gallery4.jpg" class="cbp-caption cbp-lightbox" title="Image 4">
											<span><img class="img-responsive" src="/img-temp/footer__gallery4.jpg" alt=""></span>
										</a>
									</li>
									<li class="img-hover-v2">
										<a href="/img-temp/gallery5.jpg" class="cbp-caption cbp-lightbox" title="Image 5">
											<span><img class="img-responsive" src="/img-temp/footer__gallery5.jpg" alt=""></span>
										</a>
									</li>
									<li class="img-hover-v2">
										<a href="/img-temp/gallery6.jpg" class="cbp-caption cbp-lightbox" title="Image 6">
											<span><img class="img-responsive" src="/img-temp/footer__gallery6.jpg" alt=""></span>
										</a>
									</li>
								</ul>
								<!-- End Photostream -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- End Footer -->
	</main>

	<!-- JS Global Compulsory -->
	<script src="/plugins/jquery/jquery.min.js"></script>
	<script src="/plugins/jquery/jquery-migrate.min.js"></script>
	<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>

	<!-- JS Implementing Plugins -->
	<script src="/plugins/smoothScroll.js"></script>
	<script src="/plugins/jquery.easing.min.js"></script>
	<script src="/plugins/pace/pace.min.js"></script>
	<script src="/plugins/owl-carousel2/owl.carousel.min.js"></script>
	<script src="/plugins/counter/waypoints.min.js"></script>
	<script src="/plugins/counter/jquery.counterup.min.js"></script>
	<script src="/plugins/circles-master/circles.min.js"></script>
	<script src="/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>
	<script src="/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
	<script src="/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
	<script src="/plugins/modernizr.js"></script>

	<!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?signed_in=true&amp;callback=initMap" async defer></script><script src="/js/plugins/gmaps-ini.js"></script>

	<!-- JS Page Level-->
	<script src="/js/one.app.js"></script>
	<script src="/js/plugins/owl2-carousel-v1.js"></script>
	<script src="/js/plugins/owl2-carousel-v2.js"></script>
	<script src="/js/plugins/owl2-carousel-v3.js"></script>
	<script src="/js/plugins/owl2-carousel-v4.js"></script>
	<script src="/js/plugins/owl2-carousel-v5.js"></script>
	<script src="/js/plugins/cube-portfolio.js"></script>
	<script src="/js/plugins/gmaps-ini.js"></script>
	<script src="/js/forms/contact.js"></script>
	<script>
		$(function() {
			App.init();
			App.initCounter();
			Owl2Carouselv1.initOwl2Carouselv1();
			Owl2Carouselv2.initOwl2Carouselv2();
			Owl2Carouselv3.initOwl2Carouselv3();
			Owl2Carouselv4.initOwl2Carouselv4();
			Owl2Carouselv5.initOwl2Carouselv5();
			ContactForm.initContactForm();
		});
	</script>
	<!--[if lt IE 10]>
	  <script src="..//plugins/sky-forms-pro/skyforms/js/jquery.placeholder.min.js"></script>
	<![endif]-->
    </body>
</html>
