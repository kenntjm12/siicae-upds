<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComparationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comparation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id1');
            $table->unsignedInteger('product_id2');

            $table->timestamps();

            $table->foreign('product_id1')->references('id')->on('products');
            $table->foreign('product_id2')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comparation');
    }
}
